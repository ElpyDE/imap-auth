<?php
/*
Admin pages for IMAP Authentication plugin
TODO: i18n
*/

defined( 'IMAP_AUTH_PLUGIN_PATH' ) or die( 'No script kiddies please!' );


// Settings Page:
add_action('admin_menu', 'imap_auth_admin_add_page');
function imap_auth_admin_add_page() {
    add_options_page(
        __('IMAP Authentication Settings','imap-auth'), // page title
        __('IMAP Authentication','imap-auth'), // menu title
        'edit_users', // capability
        'imap_auth', // menu slug (page)
        'imap_auth_options_page' // page callback
    );
}


function imap_auth_options_page() {
?>
    <div class="wrap">
    <h1><?php _e('IMAP Authentication Settings','imap-auth') ?></h1>
    <form action="options.php" method="post">
    <?php settings_fields('imap_auth'); ?>
    <?php do_settings_sections('imap_auth'); ?>
    <input name="Submit" type="submit" value="<?php esc_attr_e('Save Changes','imap-auth') ?>" />
    </form>
    </div>
<?php
}


// Options definition:
add_action('admin_init', 'imap_auth_admin_init');
function imap_auth_admin_init(){
    register_setting(
        'imap_auth', // option group
        'imap_auth', // option name
        'imap_auth_options_validate' // sanitize callback
    );
    // Section: General options
    add_settings_section(
        'imap_auth_main_hook', // section id
        __('Plugin options','imap-auth'), // title
        'imap_auth_sectiontext_main_hook', // section callback
        'imap_auth' // page
    );
    add_settings_field(
        'imap_auth_authenticate_priority', // field id
        __('Authenticate filter priority','imap-auth'), // title
        'imap_auth_field_number', // input callback
        'imap_auth', // page
        'imap_auth_main_hook', // section id
        array(
            'key' => 'authenticate_priority',
            'default' => 30,
            'min' => 0, 'max' => 90, 'step' => 10,
            'hint' => __('E.g. 10 = <em>before</em> | 30 = <em>after</em> regular authentication'),
        )
    );
    add_settings_field(
        'imap_auth_disable_regular_filters', // field id
        __('Disable regular authentication','imap-auth'), // title
        'imap_auth_field_checkbox_single', // input callback
        'imap_auth', // page
        'imap_auth_main_hook', // section id
        array(
            'key' => 'disable_regular_filters',
            'default' => false,
            'label' => __('Disable the regular WordPress authentication against passwords saved in the database.','imap-auth'),
            'hint' => __('<strong style="color:red">WARNING:</strong> Make sure you can login <em>before</em> you enable this!','imap-auth') . '<br />' . __('This will disable regular authentication filters and also prohibit password changing and retrieval for all users.','imap-auth'),
        )
    );
    add_settings_field(
        'imap_auth_disable_regular_auth_after_success', // field id
        __('Disable regular auth on success','imap-auth'), // title
        'imap_auth_field_checkbox_single', // input callback
        'imap_auth', // page
        'imap_auth_main_hook', // section id
        array(
            'key' => 'disable_regular_auth_after_success',
            'default' => false,
            'label' => __('Disable the regular WordPress authentication for users who successfully logged in with IMAP credentials.','imap-auth'),
            'hint' => __('This is a weaker option than the one above and especially interesting while migrating from WordPress to IMAP authentication or in mixed environments.','imap-auth'),
        )
    );
    add_settings_field(
        'imap_auth_main_hook-user_match_method', // field id
        __('User matching method','imap-auth'), // title
        'imap_auth_field_radio', // input callback
        'imap_auth', // page
        'imap_auth_main_hook', // section id
        array(
            'key' => 'user_match_method',
            'default' => 'login',
            'options' => array(
                'login' => __('Match user by login','imap-auth'),
                'email' => __('Match user by e-mail','imap-auth'),
                'login_and_email' => __('Match user by login and e-mail','imap-auth'),
            ),
            'hint' => __('Choose which of the fields (filtered as below) will be used to find a matching WordPress user.','imap-auth'),
        )
    );
    add_settings_field(
        'imap_auth_auto_user_creation', // field id
        __('Auto user creation','imap-auth'), // title
        'imap_auth_field_checkbox_single', // input callback
        'imap_auth', // page
        'imap_auth_main_hook', // section id
        array(
            'key' => 'auto_user_creation',
            'default' => false,
            'label' => __('Create a new wordpress user with "login" and "email" on credential validation if no existing user matched.','imap-auth'),
            'hint' => __('Make sure, your settings are fine and fitting "login" and "email" are produced by your filter setting below!','imap-auth'),
        )
    );
    // Section: IMAP Server
    add_settings_section(
        'imap_auth_server', // section id
        __('IMAP Server','imap-auth'), // title
        'imap_auth_sectiontext_server', // section callback
        'imap_auth' // page
    );
    add_settings_field(
        'imap_auth_server-server', // field id
        __('IMAP Server','imap-auth'), // title
        'imap_auth_field_text', // input callback
        'imap_auth', // page
        'imap_auth_server', // section id
        array(
            'key' => 'server',
            'default' => 'localhost',
            'hint' => __('Hostname or IP','imap-auth'),
        )
    );
    add_settings_field(
        'imap_auth_server-port', // field id
        __('Port','imap-auth'), // title
        'imap_auth_field_text', // input callback
        'imap_auth', // page
        'imap_auth_server', // section id
        array(
            'key' => 'port',
            'default' => '143',
            'hint' => __('Default ports: 993 = with SSL | 143 = without SSL','imap-auth'),
        )
    );
    add_settings_field(
        'imap_auth_server-flags', // field id
        __('Flags','imap-auth'), // title
        'imap_auth_field_checkbox_multi', // input callback
        'imap_auth', // page
        'imap_auth_server', // section id
        array(
            'key' => 'flags',
            'default' => ['novalidate-cert'],
            'options' => array(
                'ssl' => __('Use Secure Socket Layer (ssl)','imap-auth'),
                'notls' => __('No start-TLS encryption (notls)','imap-auth'),
                'novalidate-cert' => __('Skip certificate validation (novalidate-cert)','imap-auth'),
            ),
        )
    );
    // Section: Username to Login
    add_settings_section(
        'imap_auth_login', // section id
        __('Username to Login','imap-auth'), // title
        'imap_auth_sectiontext_login', // section callback
        'imap_auth' // page
    );
    add_settings_field(
        'imap_auth_login_pattern', // field id
        __('Login regex pattern','imap-auth'), // title
        'imap_auth_field_text', // input callback
        'imap_auth', // page
        'imap_auth_login', // section id
        array('key' => 'login_pattern',
              'default' => '',
        )
    );
    add_settings_field(
        'imap_auth_login_replace', // field id
        __('Login regex replace','imap-auth'), // title
        'imap_auth_field_text', // input callback
        'imap_auth', // page
        'imap_auth_login', // section id
        array('key' => 'login_replace',
              'default' => '',
        )
    );
    add_settings_field(
        'imap_auth_login_tolower', // field id
        __('Login to lower case','imap-auth'), // title
        'imap_auth_field_checkbox_single', // input callback
        'imap_auth', // page
        'imap_auth_login', // section id
        array('key' => 'login_tolower',
              'default' => false,
              'label' => __('Convert the login to lower case?','imap-auth'),
              'hint' => '',
        )
    );
    add_settings_field(
        'imap_auth_login_capitalize', // field id
        __('Capitalize login','imap-auth'), // title
        'imap_auth_field_checkbox_single', // input callback
        'imap_auth', // page
        'imap_auth_login', // section id
        array('key' => 'login_capitalize',
              'default' => false,
              'label' => __('Make the first character uppercase?','imap-auth'),
              'hint' => '',
        )
    );
    // Section: Username to Email
    add_settings_section(
        'imap_auth_email', // section id
        __('Username to email','imap-auth'), // title
        'imap_auth_sectiontext_email', // section callback
        'imap_auth' // page
    );
    add_settings_field(
        'imap_auth_email_pattern', // field id
        __('Email regex pattern','imap-auth'), // title
        'imap_auth_field_text', // input callback
        'imap_auth', // page
        'imap_auth_email', // section id
        array('key' => 'email_pattern',
              'default' => '',
              'hint' => __('Regex pattern for email. Will use <code>^.*$</code> (matches everything) if left empty.','imap-auth'),
        )
    );
    add_settings_field(
        'imap_auth_email_replace', // field id
        __('Email regex replace','imap-auth'), // title
        'imap_auth_field_text', // input callback
        'imap_auth', // page
        'imap_auth_email', // section id
        array('key' => 'email_replace',
              'default' => '',
              'hint' => __('Regex replacement to form email','imap-auth'),
        )
    );
    add_settings_field(
        'imap_auth_email_tolower', // field id
        __('Email to lower case','imap-auth'), // title
        'imap_auth_field_checkbox_single', // input callback
        'imap_auth', // page
        'imap_auth_email', // section id
        array('key' => 'email_tolower',
              'default' => false,
              'label' => __('Convert the login to lower case?','imap-auth'),
              'hint' => '',
        )
    );
    // Section: IMAP Login
    add_settings_section(
        'imap_auth_imaplogin', // section id
        __('IMAP login','imap-auth'), // title
        'imap_auth_sectiontext_imaplogin', // section callback
        'imap_auth' // page
    );
    add_settings_field(
        'imap_auth_imaplogin-imaplogin_from', // field id
        __('IMAP login origin','imap-auth'), // title
        'imap_auth_field_radio', // input callback
        'imap_auth', // page
        'imap_auth_imaplogin', // section id
        array(
            'key' => 'imaplogin_from',
            'default' => 'username',
            'options' => array(
                'user_login' => __('Start with login (WordPress user name)','imap-auth'),
                'user_email' => __('Start e-mail of the matched user (value from database)','imap-auth'),
                'username' => __('Start with username (as given in the input field)','imap-auth'),
            ),
            'hint' => __('Choose which field will be used as input to the below filters.','imap-auth'),
        )
    );
    add_settings_field(
        'imap_auth_imaplogin_pattern', // field id
        __('IMAP Login regex pattern','imap-auth'), // title
        'imap_auth_field_text', // input callback
        'imap_auth', // page
        'imap_auth_imaplogin', // section id
        array('key' => 'imaplogin_pattern',
              'default' => '',
        )
    );
    add_settings_field(
        'imap_auth_imaplogin_replace', // field id
        __('IMAP Login regex replace','imap-auth'), // title
        'imap_auth_field_text', // input callback
        'imap_auth', // page
        'imap_auth_imaplogin', // section id
        array('key' => 'imaplogin_replace',
              'default' => '',
        )
    );
    add_settings_field(
        'imap_auth_imaplogin_tolower', // field id
        __('IMAP login to lower case','imap-auth'), // title
        'imap_auth_field_checkbox_single', // input callback
        'imap_auth', // page
        'imap_auth_imaplogin', // section id
        array('key' => 'imaplogin_tolower',
              'default' => false,
              'label' => __('Convert the IMAP login to lower case?','imap-auth'),
              'hint' => '',
        )
    );
}


function imap_auth_sectiontext_main_hook() {
?>
    <p>This plugin uses the <a href="https://developer.wordpress.org/reference/hooks/authenticate/"><em>authenticate</em> filter</a> to hook into the authentication process.
       The regular authentication of wordpress will remain active in parallel until you disable it.</p>
<?php
}


function imap_auth_sectiontext_server() {
    echo '<p>';
    _e('Set up your IMAP Server here. The configuration is very much like how you set up your mail client.','imap-auth');
    echo '<br />';
    _e('See documentation of PHP <a href="http://php.net/manual/en/function.imap-open.php">imap_open()</a> for information about the flags and note that some combinations might not work at all.','imap-auth');
    echo '</p>';
}

function imap_auth_sectiontext_login() {
    echo '<p>' . __('Automatic transformations from username (input field) to login (WordPress user name).','imap-auth') . '</p>';
}

function imap_auth_sectiontext_email() {
    echo '<p>' . __('Automatic transformations from username (input field) to user email.','imap-auth') . '</p>';
}

function imap_auth_sectiontext_imaplogin() {
    echo '<p>' . __('Automatic transformations from login (WordPress user name) to IMAP login.','imap-auth') . '</p>';
}


function imap_auth_field_text($args) {
    $key = $args['key'];
    $value = imap_auth_opt($key, $args['default']);
?>
    <input id="imap_auth[<?php echo $key; ?>]"
    name="imap_auth[<?php echo $key; ?>]"
    size="40" type="text"
    value="<?php echo $value; ?>" />
<?php
    if (array_key_exists('hint', $args)) {
        echo '<br />' . $args['hint'];
    }
}


function imap_auth_field_number($args) {
    $key = $args['key'];
    $value = imap_auth_opt($key, $args['default']);
?>
    <input id="imap_auth[<?php echo $key; ?>]"
    name="imap_auth[<?php echo $key; ?>]"
    size="5" type="number"
    value="<?php echo $value; ?>"
    <?php echo array_key_exists('min', $args) ? 'min="' . $args['min'] . '"' : ''; ?>
    <?php echo array_key_exists('max', $args) ? 'max="' . $args['max'] . '"' : ''; ?>
    <?php echo array_key_exists('step', $args) ? 'step="' . $args['step'] . '"' : ''; ?>
    />
<?php
    if (array_key_exists('hint', $args)) {
        echo '<br />' . $args['hint'];
    }
}


function imap_auth_field_checkbox_single($args) {
    $key = $args['key'];
    $value = imap_auth_opt($key, $args['default']);
    $label = isset($args['label']) ? $args['label'] : '';
?>
    <p>
    <input id="imap_auth[<?php echo $key; ?>]"
    name="imap_auth[<?php echo $key; ?>]"
    type="checkbox"
    value="1"
    <?php checked($value); ?>/>
    <label for="imap_auth[<?php echo $key; ?>]"><?php echo $label; ?></label>
    </p>
<?php
    if (array_key_exists('hint', $args)) {
        echo ' ' . $args['hint'];
    }
}


function imap_auth_field_checkbox_multi($args) {
    $group_key = $args['key'];
    $group_arr = imap_auth_opt($group_key, $args['default']);
    $options = $args['options'];
    foreach ( $options as $option_key => $option_label ):
        $key = $group_key . "_" . $option_key;
?>
    <p>
    <input id="imap_auth[<?php echo $key; ?>]"
    name="imap_auth[<?php echo $key; ?>]"
    type="checkbox"
    value="1"
    <?php checked(array_search($option_key, $group_arr) !== false); ?>/>
    <label for="imap_auth[<?php echo $key; ?>]"><?php echo $option_label; ?></label>
    </p>
<?php
    endforeach;
    if (array_key_exists('hint', $args)) {
        echo '<br />' . $args['hint'];
    }
}


function imap_auth_field_radio($args) {
    $group_key = $args['key'];
    $value = imap_auth_opt($group_key, $args['default']);
    $options = $args['options'];
    foreach ( $options as $option_key => $option_label ):
        $key = $group_key . "_" . $option_key;
?>
    <p>
    <input id="imap_auth[<?php echo $key; ?>]"
    name="imap_auth[<?php echo $group_key; ?>]"
    type="radio"
    value="<?php echo $option_key; ?>"
    <?php checked($option_key === $value); ?>/>
    <label for="imap_auth[<?php echo $key; ?>]"><?php echo $option_label; ?></label>
    </p>
<?php
    endforeach;
    if (array_key_exists('hint', $args)) {
        echo '<br />' . $args['hint'];
    }
}


function imap_auth_options_validate($in) {
    $out = array();
    // Section: General options
    $out['authenticate_priority'] = ('' !== trim($in['authenticate_priority'])) ? intval($in['authenticate_priority']) : 30;
    $out['disable_regular_filters'] = (1 == $in['disable_regular_filters']) ? true : false;
    $out['disable_regular_auth_after_success'] = (1 == $in['disable_regular_auth_after_success']) ? true : false;
    $out['auto_user_creation'] = (1 == $in['auto_user_creation']) ? true : false;
    $out['user_match_method'] = array_search($in['user_match_method'], ['login', 'email', 'login_and_email']) !== false ? $in['user_match_method'] : 'login';
    // Section: IMAP Server
    $out['server'] = trim($in['server']);
    $out['port'] = trim($in['port']);
    $out['flags'] = [];
    $available_flags = ['ssl', 'notls', 'novalidate-cert'];
    foreach ($available_flags as $flag) {
        $flag_key = 'flags_' . $flag;
        if ( array_key_exists($flag_key, $in) && 1 == $in[$flag_key] ) {
            $out['flags'][] = $flag;
        }
    }
    // Section: Username to Login
    $out['login_pattern'] = trim($in['login_pattern']);
    $out['login_replace'] = trim($in['login_replace']);
    $out['login_tolower'] = (1 == $in['login_tolower']) ? true : false;
    $out['login_capitalize'] = (1 == $in['login_capitalize']) ? true : false;
    // Section: Username to Email
    $out['email_pattern'] = trim($in['email_pattern']);
    $out['email_replace'] = trim($in['email_replace']);
    $out['email_tolower'] = (1 == $in['email_tolower']) ? true : false;
    // Section: Username/Login/Email to IMAP Login
    $out['imaplogin_from'] = array_search($in['imaplogin_from'], ['user_login', 'user_email', 'username']) !== false ? $in['imaplogin_from'] : 'username';
    $out['imaplogin_pattern'] = trim($in['imaplogin_pattern']);
    $out['imaplogin_replace'] = trim($in['imaplogin_replace']);
    $out['imaplogin_tolower'] = (1 == $in['imaplogin_tolower']) ? true : false;
    return $out;
}

