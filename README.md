# Flexible IMAP Authentication

* Contributers: [ElpyDE](https://gitlab.com/ElpyDE)
* Tags: IMAP, Authentication, Single Sign-On
* Requires at least: 4.8
* Tested up to: 4.8.2
* Stable tag: master
* License: GPLv3 or later
* License URI: http://www.gnu.org/licenses/gpl.html

This plugin allows users to login with the same credentials they use to access 
an IMAP server. It should be flexible enough to cover nearly every use case.

## Description

__This plugin is in early development - you may use is for testing__

This plugin allows users to login with the same credentials they use to access 
an IMAP server.

There may be as many different use cases for IMAP logins to WordPress sites as
there are WordPress sites. The aim of this plugin is to allow you to match your
use case. It does so by the use of several filters to map whatever the user is
supposed to enter as username to the right WordPress user and then to the right
server and settings.

__Features__

* Auto remove/add prefix/postfix (such as the domain)
* Customize (next to) any step with your own filter function without the need to
  worry about the rest of the process.

__Future Plans__

* Auto create users on successful login
* Support multiple servers

## Installation

Best is to install this plugin is via the [GitHub Updater][github-updater] plugin.
This also ensures you get updates for this plugin:

1. Install [GitHub Updater][github-updater]
   (see [GitHub Updater Installation][github-updater-install])
2. Go to Settings -> GitHub Updater and choose the tab "Install Plugin"
3. Use the following information:  
   * Plugin URI: https://gitlab.com/ElpyDE/imap-auth
   * Repository Branch: *leave empty*
   * Remote Repository Host: GitLab
   * GitLab Access Token: *leave empty*
4. Go to Settings -> IMAP Authentication

[github-updater]: https://github.com/afragen/github-updater
[github-updater-install]: https://github.com/afragen/github-updater/wiki/Installation

