<?php
/*
Plugin Name:       Flexible IMAP Authentication
Plugin URI:        https://gitlab.com/ElpyDE/imap-auth
Description:       A flexible IMAP authentication mechanism
Version:           0.0.6
Author:            ElpyDE
Author URI:        https://gitlab.com/ElpyDE
License:           GNU General Public License (GPLv3) or later
License URI:       http://www.gnu.org/licenses/gpl.html
GitLab Plugin URI: https://gitlab.com/ElpyDE/imap-auth
Text Domain:       imap-auth
*/

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

if ( ! defined('IMAP_AUTH_PLUGIN_PATH') ) {
    define('IMAP_AUTH_PLUGIN_PATH', plugin_dir_path( __FILE__ ));
}
if ( ! defined('IMAP_AUTH_PLUGIN_URL') ) {
    define('IMAP_AUTH_PLUGIN_URL', plugin_dir_path( __FILE__ ));
}

/**
 * Admin Pages
 **/
add_action( 'init', 'imap_auth_admin_pages' );
function imap_auth_admin_pages() {
    if ( current_user_can('administrator') ) {
        require_once( IMAP_AUTH_PLUGIN_PATH . 'admin-settings.php');
    }
}

/**
 * Get cached option or default value
 **/
function imap_auth_opt($key, $default = false) {
    $options = get_option('imap_auth');
    if (is_array($options) && array_key_exists($key, $options)) {
        return $options[$key];
    }
    return $default;
}

/**
 * Add filters, partially depending on options
 **/
add_action('plugins_loaded', 'imap_auth_add_filters');
function imap_auth_add_filters() {
    // The core functionality of this plugin is in the authenticate filter.
    $authenticate_priority = imap_auth_opt('authenticate_priority', 30);
    add_filter('authenticate', 'imap_auth_authenticate', $authenticate_priority, 3);
    
    add_filter('imap_auth_username_to_login', 'imap_auth_username_to_login', 10, 1);
    add_filter('imap_auth_username_to_email', 'imap_auth_username_to_email', 10, 1);

    add_filter('imap_auth_get_user', 'imap_auth_get_user', 10, 3);
    add_filter('imap_auth_get_imaplogin', 'imap_auth_get_imaplogin', 10, 3);
    add_filter('imap_auth_get_imapoptions', 'imap_auth_get_imapoptions', 10, 3);

    if ( imap_auth_opt('auto_user_creation', false) ) {
        add_filter('imap_auth_get_user', 'imap_auth_create_user', 50, 3);
    }

    add_action('imap_auth_login_success', 'imap_auth_login_success');

    add_filter('show_password_fields', 'imap_auth_show_password_fields', 10, 2);
    add_filter('allow_password_reset', 'imap_auth_allow_password_reset', 10, 2);

    add_filter( 'manage_users_columns', function ( $columns ) {
        $columns['imap_auth'] = 'IMAP auth';
        return $columns;
    });
    add_filter( 'manage_users_custom_column',function ( $val, $column_name, $user_id ) {
        switch ($column_name) {
            case 'imap_auth':
                $uses_imap = get_user_meta($user_id, 'imap_auth_uses_imap_auth', true);
                if ( ! $uses_imap ) {
                    return '&#x274C;'; // red crossmark
                }
                $last_login = get_user_meta($user_id, 'last_login', true);
                if ($last_login) {
                    return '&#x2705; ' . wp_date( get_option( 'date_format' ), intval($last_login));
                } else {
                    return '&#x2705;';
                }
            default:
        }
        return $val;
    }, 10, 3 );
    add_action('admin_head', function () {
        echo '<style>.column-imap_auth { min-width: 8rem; }</style>';
    });

    // Disable regular authentication:
    if ( imap_auth_opt('disable_regular_filters', false) ) {
        remove_filter('authenticate', 'wp_authenticate_username_password',  20, 3);
        remove_filter('authenticate', 'wp_authenticate_email_password',  20, 3);
        add_action('lost_password', 'imap_auth_disable_lost_password');
        add_action('password_reset', 'imap_auth_disable_reset_password');
    }
}

/**
 * authenticate filter
 * The core functionality of this plugin is in this filter.
 **/
function imap_auth_authenticate($user, $username, $password) {
    // User already authenticated by another plugin:
    if ( $user instanceof WP_User ) {
        return $user;
    }
    // Credentials incomplete, return errors like wp_authenticate_username_password
    if ( empty($username) || empty($password) ) {
        if ( is_wp_error( $user ) ) {
            return $user;
        }
        $error = new WP_Error();
        if ( empty($username) ) {
            $error->add('empty_username', __('<strong>ERROR</strong>: The username field is empty.'));
        }
        if ( empty($password) ) {
            $error->add('empty_password', __('<strong>ERROR</strong>: The password field is empty.'));
        }
        return $error;
    }
    // Reset $user to NULL, i.e. ignore previous errors
    $user_orig = $user;
    $user = NULL;
    
    // Step 1: Get the login and email used for matching/creating a user
    $login = apply_filters('imap_auth_username_to_login', $username);
    $email = apply_filters('imap_auth_username_to_email', $username);

    if ( is_null($login) || is_null($email) ) {
        return new WP_Error('imap_authentication_regex_nomatch', __('The username is not allowed to login.','imap-auth'));
    }

    // Step 2: Match a user or create one, if this is enabled
    $user = apply_filters('imap_auth_get_user', NULL, $login, $email);

    // Exit point: No matching user was found and user creation is turned off
    if ( is_null($user) || is_wp_error($user) ) {
        return $user;
    }

    // Step 3: Collect everything we need to login to the IMAP server

    // The $imaplogin will be used as username in the IMAP connection:
    $imaplogin = apply_filters('imap_auth_get_imaplogin', $username, $user);

    // $imapoptions is an array with all info needed for the mailbox string in imap_open()
    $imapoptions = array(
        'server' => 'localhost',
        'port' => '143',
        'service' => 'imap',
        'flags' => array('readonly'),
    );
    $imapoptions = apply_filters('imap_auth_get_imapoptions', $imapoptions, $user);

    // Make up the $mailbox string for imap_open():
    $flags_str = empty($imapoptions['flags']) ? '' : '/' . implode('/', $imapoptions['flags']);
    $mailbox = '{' . $imapoptions['server'] . ':' . $imapoptions['port'] . '/service=' . $imapoptions['service'] . $flags_str . '}INBOX';

    // Step 4: Try connect to IMAP server
    $mbox = @imap_open($mailbox, $imaplogin, $password, OP_HALFOPEN|OP_READONLY, 0 );
    if ( ! $mbox ) {
        // IMAP login failed
        // Clear IMAP errors:
        if ($imap_errors = imap_errors() && WP_DEBUG === true ) {
            error_log( $mailbox . ' user=' . $imaplogin . "\n" . implode("\n", $imap_errors));
        }
        return new WP_Error('imap_authentication_failed', 'Authentication using IMAP failed.');
    }
    // IMAP login successful, close connection and go on...
    imap_close($mbox);
    
    // Step 5: Insert newly created user to the database:
    if ( ! $user->exists() ) {
        $user_id = wp_insert_user( $user );
        if ( is_wp_error($user_id) ) {
            // Could not insert the user into the database
            return new WP_Error('imap_auth_user_insert_failed', __('User creation failed.', 'imap-auth'));
        }
        // Finally, get the user populated with all properties:
        $user = get_user_by('id', $user_id);
        // Redirect newly created users to their profile page:
        add_filter('login_redirect', 'imap_auth_new_user_goto');
    }

    do_action('imap_auth_login_success', $user);

    // Step 6: We are done!
    return $user;
}

/**
 * Helper function for filters
 **/
function _imap_auth_x_to_y($x, $s) {
    if ( empty( $pattern = imap_auth_opt($s . '_pattern', '') ) ) {
        $pattern = '(.*)';
    }
    if ( empty( $replace = imap_auth_opt($s . '_replace', '') ) ) {
        $replace = '${1}';
    }
    $x = preg_replace('/^' . $pattern . '$/', $replace, $x, -1, $replace_count);
    if ( $replace_count !== 1 ) { return NULL; }
    if ( imap_auth_opt($s . '_tolower', false) ) {
        $x = strtolower($x);
    }
    if ( imap_auth_opt($s . '_capitalize', false) ) {
        $x = ucfirst($x);
    }
    return $x;
}

/**
 * Filter: imap_auth_username_to_login
 *
 **/
function imap_auth_username_to_login($username) {
    return _imap_auth_x_to_y($username, 'login');
}

/**
 * Filter: imap_auth_username_to_email
 *
 **/
function imap_auth_username_to_email($username) {
    return _imap_auth_x_to_y($username, 'email');
}

/**
 * Filter: imap_auth_get_user
 *
 **/
function imap_auth_get_user($user, $login, $email) {
    $method = imap_auth_opt('user_match_method', 'login');
    switch ( $method ) {
        case 'login':
            $user_by_login = get_user_by('login', $login);
            //return new WP_Error('ia-debug', var_export([$login,$email,$method,$user_by_login],true));
            if ( $user_by_login instanceof WP_User ) {
                $user = $user_by_login;
            }
            break;
        case 'email':
            $user_by_email = get_user_by('email', $email);
            if ( $user_by_email instanceof WP_User ) {
                $user = $user_by_email;
            }
            break;
        case 'login_and_email':
            $user_by_login = get_user_by('login', $login);
            if ( $user_by_login instanceof WP_User && $user_by_login->user_email === $email ) {
                $user = $user_by_login;
            }
            break;
    }
    return $user;
}

/**
 * Filter: imap_auth_get_user
 * Create a new user that will (on success) later be inserted into database
 **/
function imap_auth_create_user($user, $login, $email) {
    if ( is_null($user) ) {
        $user = new WP_User();
        $user->user_login = $login;
        $user->user_email = $email;
    }
    return $user;
}


/**
 * Filter: imap_auth_get_imaplogin
 *
 **/
function imap_auth_get_imaplogin($imaplogin, $user) {
    switch ( imap_auth_opt('imaplogin_from', 'username') ) {
        case 'user_email':
            $imaplogin = $user->user_email;
            break;
        case 'user_login':
            $imaplogin = $user->user_login;
            break;
        case 'username':
            //$imaplogin = $imaplogin;
            break;
    }
    return _imap_auth_x_to_y($imaplogin, 'imaplogin');
}

/**
 * Filter: imap_auth_get_imapoptions
 *
 **/
function imap_auth_get_imapoptions($imapoptions, $user) {
    // Server:
    $imapoptions['server'] = imap_auth_opt('server', $imapoptions['server']);
    // Port:
    $imapoptions['port'] = imap_auth_opt('port', $imapoptions['port']);
    // Flags:
    $imapoptions['flags'] = array_merge($imapoptions['flags'], imap_auth_opt('flags', ['novalidate-cert']));
    return $imapoptions;
}

/**
 * Filter: login_redirect
 * Redirect (newly created) user to its profile page
 **/
function imap_auth_new_user_goto() {
    return '/wp-admin/profile.php';
}

/**
 *
 **/
function imap_auth_login_success($user) {
    $user_id = $user->ID;
    if ( empty($user_meta = get_user_meta($user_id, 'imap_auth_uses_imap_auth', true) ) || !$user_meta ) {
        if ( imap_auth_opt('disable_regular_auth_after_success', false) ) {
            // Remember that this user _only_ should use IMAP authentication
            update_user_meta($user_id, 'imap_auth_uses_imap_auth', true);
            // overwrite password with random password:
            wp_set_password(wp_generate_password(20, true, true), $user_id);
        }
    }
    update_user_meta($user_id, 'last_login', time());
}

/**
 *
 **/
function imap_auth_show_password_fields( $show, $profileuser ) {
    if ( imap_auth_opt('disable_regular_filters', false) ) {
        print '</table><h2>';
        _e('Account Management');
        print '</h2><table class="form-table"><tr class="user-imap_auth-wrap"><th>';
        _e('IMAP Authentication','imap-auth');
        print '</th><td>';
        _e('Regular WordPress authentication was completely disabled in favor of IMAP authentication. WordPress password management is disabled.','imap-auth');
        print '</td></tr>';
        return false;
    } elseif ( imap_auth_opt('disable_regular_auth_after_success', false) && get_user_meta($profileuser->ID, 'imap_auth_uses_imap_auth', true) ) {
        print '</table><h2>';
        _e('Account Management');
        print '</h2><table class="form-table"><tr class="user-imap_auth-wrap"><th>';
        _e('IMAP Authentication','imap-auth');
        print '</th><td>';
        _e('This user logged in successfully with his/her IMAP credentials. WordPress password management is disabled.','imap-auth');
        print '</td></tr>';
        return false;
    }
    return $show;
}

/**
 *
 **/
function imap_auth_allow_password_reset( $allow, $user_id ) {
    if ( imap_auth_opt('disable_regular_auth_after_success', false) && !empty($user_meta = get_user_meta($user_id, 'imap_auth_uses_imap_auth', true) ) && $user_meta ) {
        return false;
    } else {
        return (imap_auth_opt('disable_regular_filters', false)) ? false : $allow;
    }
    return $allow;
}

/**
 * 
 **/
function imap_auth_disable_lost_password() {
    login_header( __('Lost Password'), '<p class="message">' . __('Password reset/retrieval is not available. Use the login credentials of your e-mail account.','imap-auth') . '</p>', '' );
    ?>
    <p id="nav"><a href="<?php echo wp_login_url(); ?>" title="<?php esc_attr_e( 'Try again!' ); ?>"><?php printf( __( 'Log In' ) ); ?></a></p>
    <?php
    login_footer();
    die();
}

function imap_auth_disable_reset_password() {
    wp_die(
        __('Password reset is disabled on this site. Ask your administrator if you feel this is not correct.', 'imap-auth'),
        __('Password reset disabled'),
        array(
            'response' => '403',
            'back_link' => true,
        )
    );
}

